<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<title>The Insights Guide</title>
    	<script>
			var pg_settings = { 
			'url':'/connector',
			'token': '<?=$res->token;?>',
			'oncomplete': ''  
			}; 
	</script>
	<script src='https://api.<?=$environment;?>/pg-js'></script>
	<link href='https://api.<?=$environment;?>/pg-css' rel='stylesheet' type='text/css'>
	<link href='/bin/css/guide.min.css' rel='stylesheet' type='text/css'>
    <meta name = "viewport" content = "initial-scale=1.0, maximum-scale=1.0, width=device-width">         
	</head>
	<body></body>
</html>
