<?

	//DEVELOPMENT OR PRODUCTION
	$dev = true;
	$environment = $dev ? 'ppi.dev' : 'ppi.co';

?>
<!doctype html>
	<html>
		<head>
		<meta charset="utf-8">
		<title>Package Manager</title>
			<script>
				var pm_settings = { 
					'url':'/connector', 
					'init': 'package/_demo/edit', 
					'onstart': 'pkg.set', 
					'onsave': function () { window.close(); } 
					}; 
			 </script>
			<script src="https://api.<?=$environment;?>/pm-js"></script>
			<link href="https://api.<?=$environment;?>/pm-css" rel="stylesheet" type="text/css">
			<link href="/bin/css/pm.min.css" rel="stylesheet" type="text/css">
		</head>
		<body>
			<div id='header'><div id='logo'></div></div>
		</body>
	</html>
